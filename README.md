# Prueba Work

## Introduction
El proyecto solo necesita instalar las dependencias, se usa MongoDB Atlas como base de datos y una API externa para listar los paises. 
Como proyecto principal se uso VueJS y un servidor en Express JS para conectarse con la base de datos y desarrollar el CRUD.

## Packages
Vue JS, Bulma CSS, Buefy and Express JS

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build && node server.js
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
