/**
 * @author J. Daza
 * @description Servidor de CRUD | Prueba
 */

//MODULES
require('dotenv').config()
const express = require('express')
const cors = require('cors')
const path = require('path')
const history = require('connect-history-api-fallback')
const mongoose = require('mongoose')

//PORTS
const PORT = process.env.PORT || 3001

//MONGO
mongoose
  .connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  })
  .then((res) => {
    console.log(`Base de datos conectada a --> ${process.env.MONGO_URL}`)
  })
  .catch((err) => {
    console.log(
      `Error al conectar con la base de datos --> ${process.env.MONGO_URL}`
    )
  })

//SCHEMA
const userSchema = new mongoose.Schema({
  name: String,
  lastname: String,
  age: Number,
  gender: {
    type: String,
    enum: ['male', 'female'],
  },
  country: String,
})

let User = mongoose.model('users', userSchema)

//VIEWS
const publicPath = path.resolve(__dirname, './dist')
const staticConf = { maxAge: '1y', etag: false }

//SERVER
const app = express()
const router = express.Router()

//CONTROLLERS

/**
 * Crear usuario
 */
router.post('/users/create', async (req, res, next) => {
  try {
    let data = req.body
    let user = User.create(data)
    res.json({ user, message: 'Usuario creado con éxito.' })
  } catch (error) {
    console.log(error)
    next(error)
  }
})

/**
 * Listar usuarios
 */
router.post('/users/list', async (req, res, next) => {
  try {
    let users = await User.find({}).lean()
    res.json({ user: users, message: 'Usuarios listados con éxito.' })
  } catch (error) {
    console.log(error)
    next(error)
  }
})

/**
 * Obtener usuario
 */
router.get('/users/:id', async (req, res, next) => {
  try {
    let { id } = req.params
    let user = await User.findById(id)
    res.json({ user, message: 'Usuario obtenido con éxito.' })
  } catch (error) {
    console.log(error)
    next(error)
  }
})

/**
 * Editar usuario
 */
router.put('/users/:id', async (req, res, next) => {
  try {
    let { id } = req.params
    let changes = req.body
    delete changes._id

    let user = await User.findByIdAndUpdate(id, changes)
    res.json({ user, message: 'Usuario editado con éxito.' })
  } catch (error) {
    console.log(error)
    next(error)
  }
})

/**
 * Eliminar usuario
 */
router.delete('/users/:id', async (req, res, next) => {
  try {
    let { id } = req.params
    let user = await User.findByIdAndDelete(id)
    res.json({ user, message: 'Usuario eliminado con éxito.' })
  } catch (error) {
    console.log(error)
    next(error)
  }
})

//MIDDLEWARE
app.use(cors())
app.use(express.json())
app.use('/api', router)
app.use(express.static(publicPath, staticConf))
app.use(history())
app.use(express.static(publicPath, staticConf))
// app.get('/', function(req, res) {
//   res.render(path.join(__dirname, 'index.html'))
// })

//RUN
app.listen(PORT, () => {
  console.log(`Servidor iniciado en el puerto --> ${PORT}`)
})
