/**
 * @description Servicio de la API de paises
 */

require('dotenv').config()
const axios = require('axios').default

module.exports = {
  getAllCountries: () => {
    return new Promise(async (resolve, reject) => {
      try {
        let { data } = await axios.get(process.env.VUE_APP_COUNTRY_API_URL)
        return resolve(data)
      } catch (error) {
        return reject(error)
      }
    })
  },
}
