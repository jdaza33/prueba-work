/**
 * @description Servicio de la API
 */

require('dotenv').config()
const axios = require('../config/axios')

module.exports = {
  getUser: (id) => {
    return new Promise(async (resolve, reject) => {
      try {
        let { data } = await axios.get(`/api/users/${id}`)
        return resolve(data)
      } catch (error) {
        return reject(error)
      }
    })
  },
  listUsers: () => {
    return new Promise(async (resolve, reject) => {
      try {
        let { data } = await axios.post(`/api/users/list`)
        return resolve(data)
      } catch (error) {
        return reject(error)
      }
    })
  },
  createUser: (user) => {
    return new Promise(async (resolve, reject) => {
      try {
        let { data } = await axios.post(`/api/users/create`, user)
        return resolve(data)
      } catch (error) {
        return reject(error)
      }
    })
  },
  editUser: (id, changes) => {
    return new Promise(async (resolve, reject) => {
      try {
        let { data } = await axios.put(`/api/users/${id}`, changes)
        return resolve(data)
      } catch (error) {
        return reject(error)
      }
    })
  },
  delUser: (id) => {
    return new Promise(async (resolve, reject) => {
      try {
        let { data } = await axios.delete(`/api/users/${id}`)
        return resolve(data)
      } catch (error) {
        return reject(error)
      }
    })
  },
}
